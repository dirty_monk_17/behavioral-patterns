<?php

interface Sneakers
{
    public function run(): string;
    public function jump(): string;
}

class Nike implements Sneakers
{
    public function run(): string
    {
        return 'Бег в Nike: 20 км/ч' . '<br>';
    }

    public function jump(): string
    {
        return 'Прыжок в Nike: 2 м' . '<br>' . '<br>';
    }
}

class Adidas implements Sneakers
{
    public function run(): string
    {
        return 'Бег в Adidas: 15 км/ч' . '<br>';
    }

    public function jump(): string
    {
        return 'Прыжок в Adidas: 3 м' . '<br>' . '<br>';
    }
}

class Sportsman
{
    private Sneakers $sneakers;

    public function __construct(Sneakers $sneakers)
    {
        $this->sneakers = $sneakers;
    }

    public function run(): string
    {
        return $this->sneakers->run();
    }

    public function jump(): string
    {
        return $this->sneakers->jump();
    }

    public function changeSneakers(Sneakers $sneakers) // присваиваем новое значение переменной $sneakers
    {
        $this->sneakers = $sneakers;
    }
}

$sportsman1 = new Sportsman(new Nike());
echo $sportsman1->run();
echo $sportsman1->jump();

$sportsman1->changeSneakers(new Adidas());
echo $sportsman1->run();
echo $sportsman1->jump();