<?php

class Brand
{
    protected Mediator $mediator;

    public function setMediator(Mediator $mediator)
    {
        $this->mediator = $mediator;
    }
}

class Sneakers extends Brand
{
    public function outputBrandName(string $brandName)
    {
        echo 'Brand: ' . $this->mediator->getBrand($brandName);
    }
}

class Sweater extends Brand
{
    public function getBrandName(string $brand): string
    {
        return $brand;
    }
}

class Mediator
{
    public function __construct(private Sweater $brandSweater, private Sneakers $brandSneakers)
    {
        $this->brandSweater->setMediator($this);
        $this->brandSneakers->setMediator($this);
    }

    public function printBrand(string $brand)
    {
        $this->brandSneakers->outputBrandName($brand);
    }

    public function getBrand(string $brandName): string
    {
        return $this->brandSweater->getBrandName($brandName);
    }
}

$mediator = new Mediator(new Sweater(), new Sneakers());

$mediator->printBrand('Adidas');