<?php

interface Shop
{
    public function notifyBuyers();
    public function addBuyer(Buyer $buyer);
    public function removeBuyer(Buyer $buyer);
}

interface Buyer
{
    public function update(Shop $shop);
}

class SneakersShop implements Shop
{
    private array $buyers = [];
    public string $condition;

    public function addBuyer(Buyer $buyer)
    {
        $this->buyers[] = $buyer;
    }

    public function removeBuyer(Buyer $buyer)
    {
        foreach ($this->buyers as $key => $currentBuyer) {
            if ($currentBuyer == $buyer) {
                unset($this->buyers[$key]);
                return;
            }
        }
    }

    public function setCondition(string $condition)
    {
        $this->condition = $condition;
    }

    public function notifyBuyers()
    {
        foreach ($this->buyers as $buyer) {
            if($this->condition == 'Завоз товара') {
                $buyer->update($this);
            }
        }
    }
}

class NikeBuyer implements Buyer
{
    public function update(Shop $shop)
    {
        echo 'Новая коллекция Nike уже в магазине!' . '<br>';
    }
}

class AdidasBuyer implements Buyer
{
    public function update(Shop $shop)
    {
        echo 'Новая коллекция Adidas уже в магазине!' . '<br>';
    }
}

$sneakersShop = new SneakersShop();

$client1 = new NikeBuyer();
$client2 = new NikeBuyer();
$client3 = new NikeBuyer();
$client4 = new AdidasBuyer();

$sneakersShop->addBuyer($client1);
$sneakersShop->addBuyer($client2);
$sneakersShop->addBuyer($client3);
$sneakersShop->removeBuyer($client1);
$sneakersShop->addBuyer($client4);

$sneakersShop->setCondition('Завоз товара');
//$sneakersShop->setCondition('Новое условие');

$sneakersShop->notifyBuyers();