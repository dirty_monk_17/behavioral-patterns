<?php

class Sportsman
{
    public string $name;
    public array $results = [];

    public function __construct($name, float $run100meters, float $longJump, float $highJump)
    {
        $this->name = $name;
        $this->results['run100meters'] = $run100meters;
        $this->results['longJump'] = $longJump;
        $this->results['highJump'] = $highJump;
    }
}

abstract class Qualification
{
    protected Qualification $nextTest;

    abstract public function compete(Sportsman $sportsman);
}

class Run100meters extends Qualification
{
    public function compete(Sportsman $sportsman)
    {
        if($sportsman->results['run100meters'] <= 10.5) {
            $this->nextTest = new LongJump();
            $this->nextTest->compete($sportsman);
        } else {
            echo $sportsman->name . ' не прошел 1 этап (100 м).' . '<br>';
        }
    }
}

class LongJump extends Qualification
{
    public function compete(Sportsman $sportsman)
    {
        if($sportsman->results['longJump'] >= 7) {
            $this->nextTest = new HighJump();
            $this->nextTest->compete($sportsman);
        } else {
            echo $sportsman->name . ' не прошел 2 этап (прыжок в длину).' . '<br>';
        }
    }
}

class HighJump extends Qualification
{
    public function compete(Sportsman $sportsman)
    {
        if($sportsman->results['highJump'] >= 5) {
            echo $sportsman->name . ', поздравляем! Квалификация пройдена.' . '<br>';
        } else {
            echo $sportsman->name . ' не прошел 3 этап (прыжок в высоту).' . '<br>';
        }
    }
}

$useinBolt = new Sportsman('Усейн Болт', 9.5, 8, 6);
$ivanPetrov = new Sportsman('Иван Петров', 10.3, 5, 7);
$vovaPetrenko = new Sportsman('Владимир Петренко', 11.2, 7.5, 5);

$test1 = new Run100meters();
$test1->compete($useinBolt);
$test1->compete($ivanPetrov);
$test1->compete($vovaPetrenko);