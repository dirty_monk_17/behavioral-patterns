<?php

interface Visitor
{
    public function addSneakers(Sneakers $product);
    public function addSweater(Sweater $product);
}

class ShoppingCart implements Visitor
{
    private array $products = [];

    public function addSneakers(Sneakers $product)
    {
        $this->products[] = $product;
    }

    public function addSweater(Sweater $product)
    {
        $this->products[] = $product;
    }

    public function getProducts(): array
    {
        return $this->products;
    }
}

interface Product
{
    public function accept(Visitor $visitor); // метод принятия посетителя
}

class Sneakers implements Product
{
    public function __construct(private string $name)
    {

    }

    public function getProductName(): string
    {
        return 'Кроссовки ' . $this->name . '<br>';
    }

    public function accept(Visitor $visitor)
    {
        $visitor->addSneakers($this);
    }
}

class Sweater implements Product
{
    public function __construct(private string $name)
    {

    }

    public function getProductName(): string
    {
        return 'Кофта ' . $this->name . '<br>';
    }

    public function accept(Visitor $visitor)
    {
        $visitor->addSweater($this);
    }
}

$cart = new ShoppingCart();

$product1 = new Sneakers('Nike');
$product2 = new Sneakers('Adidas');
$product3 = new Sweater('Nike');

$product1->accept($cart);
$product2->accept($cart);
$product3->accept($cart);

var_dump($cart->getProducts());

echo $product1->getProductName();
echo $product2->getProductName();
echo $product3->getProductName();