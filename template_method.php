<?php

abstract class Sneakers
{
    public function create() // скелет алгоритма изготовления кроссовок
    {
        $this->selectParts();
        $this->sewSneakers();
        $this->addBrand();
    }

    private function selectParts()
    {
        echo '1. Берем все необходимые части будущих кроссовок.' . '<br>';
    }

    private function sewSneakers()
    {
        echo '2. Штопаем кроссовки.' . '<br>';
    }

    abstract protected function addBrand();
}

class Nike extends Sneakers
{
    protected function addBrand()
    {
        echo '3. Клеим логотип "Nike".' . '<br>';
    }
}

class Adidas extends Sneakers
{
    protected function addBrand()
    {
        echo '3. Клеим логотип "Adidas".' . '<br>';
    }
}

$sneakers1 = new Nike();
$sneakers1->create();
echo '<br>';
echo '==================================';
echo '<br>';
echo '<br>';
$sneakers1 = new Adidas();
$sneakers1->create();