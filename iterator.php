<?php

interface SportIterator
{
    public function getCurrent(); // получение текущего положения итератора
    public function hasNext(): bool; // проверка: есть ли след. элемент
    public function getNext(); // переход к след. элементу
}

interface Collection
{
    public function getIterator(): SportIterator;
}

class SneakersIterator implements SportIterator
{
    private array $collection;
    private int $currentPosition = 0;

    public function __construct(array $collection)
    {
        $this->collection = $collection;
    }

    public function getCurrent()
    {
        return 'Размер: ' . $this->collection[$this->currentPosition] . '<br>';
    }

    public function hasNext(): bool
    {
        if (isset($this->collection[$this->currentPosition + 1])) {
            return true;
        } else {
            return false;
        }
    }

    public function getNext()
    {
        if ($this->hasNext()) {
            $this->currentPosition++;
            return 'Размер: ' . $this->collection[$this->currentPosition] . '<br>';
        } else {
            echo 'Кроссовок больше нет' . '<br>';
        }
    }
}

class SneakersCollection implements Collection
{
    private array $sneakersCollection;

    public function __construct(array $sneakersCollection)
    {
        $this->sneakersCollection = $sneakersCollection;
    }

    public function getIterator(): SportIterator
    {
        return new SneakersIterator($this->sneakersCollection);
    }
}

$collection = new SneakersCollection(['39', '40', '41', '42', '43']);
$iterator = $collection->getIterator();

echo $iterator->getCurrent();
echo $iterator->getNext();
echo $iterator->getNext();
echo $iterator->getNext();
echo $iterator->getNext();
echo $iterator->getNext();
echo $iterator->getNext();
echo $iterator->getNext();