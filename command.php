<?php

interface Human
{
    public function train();
}

class Sportsman implements Human
{
    public function train()
    {
        echo 'Спортсмен тренируется' . '<br>' . '<br>';
    }
}

interface Sport
{
    public function startTrain();
}

class Run implements Sport
{
    private Human $human;

    public function __construct(Human $human)
    {
        $this->human = $human;
    }

    public function startTrain()
    {
        echo 'Начинаем бегать' . '<br>';
        $this->human->train();
    }
}

class Swim implements Sport
{
    private Human $human;

    public function __construct(Human $human)
    {
        $this->human = $human;
    }

    public function startTrain()
    {
        echo 'Начинаем плавать' . '<br>';
        $this->human->train();
    }
}

class Coach
{
    private Sport $sport;

    public function chooseSport(Sport $sport)
    {
        $this->sport = $sport;
    }

    public function giveCommand()
    {
        $this->sport->startTrain();
    }
}

$coach = new Coach(); // инвокер

$sportsman = new Sportsman(); // ресивер

$swim = new Swim($sportsman); // команда 1
$run = new Run($sportsman); // команда 2

$coach->chooseSport($swim);
$coach->giveCommand();

$coach->chooseSport($run);
$coach->giveCommand();