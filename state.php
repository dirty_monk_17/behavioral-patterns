<?php

abstract class State
{
    protected ShuttleRun $shuttleRun;

    public function __construct(ShuttleRun $shuttleRun)
    {
        $this->shuttleRun = $shuttleRun;
    }

    abstract public function run();
}

class RunForward extends State
{
    public function run()
    {
        echo 'Бег от точки "А" до точки "Б"' . '<br>';

        $this->shuttleRun->useStop();
    }
}

class RunBack extends State
{
    public function run()
    {
        echo 'Бег от точки "Б" до точки "А"' . '<br>';

        $this->shuttleRun->useStop();
    }
}

class Stop extends State
{
    public function run()
    {
        echo 'Остановка и разворот на 180 градусов' . '<br>';

        if ($this->shuttleRun->isRunForward()) {
            $this->shuttleRun->useRunBack();
        } else {
            $this->shuttleRun->useRunForward();
        }
    }
}

class ShuttleRun
{
    private State $state;
    private State $runForward;
    private State $runBack;
    private State $stop;
    private State $previousState;

    public function __construct()
    {
        $this->runForward = new RunForward($this);
        $this->runBack = new RunBack($this);
        $this->stop = new Stop($this);

        $this->state = $this->runForward;
    }

    public function run()
    {
        $this->state->run();
    }

    public function useRunForward()
    {
        $this->state = $this->runForward;
    }

    public function useRunBack()
    {
        $this->state = $this->runBack;
    }

    public function useStop()
    {
        $this->previousState = $this->state;

        $this->state = $this->stop;
    }

    public function isRunForward(): bool
    {
        return $this->previousState === $this->runForward;
    }
}

$shuttleRun = new ShuttleRun();

$shuttleRun->run();
$shuttleRun->run();
$shuttleRun->run();
$shuttleRun->run();
$shuttleRun->run();