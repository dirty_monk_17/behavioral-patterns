<?php

class SneakersProduction
{
    private string $currentProduction = '';

    public function __construct(string $initProduction)
    {
        $this->currentProduction = $initProduction;
        echo 'Создано прозводство кроссовок: ' . $this->currentProduction . '<br>';
    }

    public function editProduction($newProduction)
    {
        $prevProduction = $this->currentProduction;
        $this->currentProduction = $newProduction;
        echo 'Производство изменилось с ' . $prevProduction . ' на ' . $this->currentProduction . '<br>';
    }

    public function createSave(): ProductionSave
    {
        return new ProductionSave($this->currentProduction);
    }

    public function restoreFromSave(ProductionSave $save)
    {
        $this->currentProduction = $save->getProduction();
        echo 'Производство восстановлено из снимка с датой ' . $save->getDate() . $save->getProduction() . '<br>';
    }
}

interface Save
{
    public function getName(): string;
    public function getDate(): string;
}

class ProductionSave implements Save
{
    private string $date;
    private string $savedProduction;

    public function __construct($productionVersion)
    {
        $this->savedProduction = $productionVersion;
        $this->date = date(' Y-M-D-H-i-s ');
        echo '[Новое сохранение] ' . $this->getName();
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getName(): string
    {
        return $this->date . ', сохранено прозводство: ' . $this->savedProduction . '<br>';
    }

    public function getProduction(): string
    {
        return 'Текущее производство: ' . $this->savedProduction;
    }
}

class SaveManager
{
    private array $saves = [];

    private object $production;

    public function __construct(SneakersProduction $production)
    {
        $this->production = $production;
        $this->makeSave();
    }

    public function makeSave()
    {
        $this->saves[] = $this->production->createSave();
    }

    public function undo()
    {
        if (sizeof($this->saves) >= 2) {
            $this->production->restoreFromSave($this->saves[sizeof($this->saves) - 2]);
            unset($this->saves[sizeof($this->saves) - 1]);
        } else {
            echo 'У вас есть только одно исходное производство, восстанавливать нечего.' . '<br>' . '<br>';
        }
    }

    public function changeProduction(string $newProduction)
    {
        $this->production->editProduction($newProduction);
        $this->makeSave();
    }
}

$production1 = new SneakersProduction("ВЕРСИЯ 1.0");
$saveManager1 = new SaveManager($production1);
$saveManager1->undo();

$production2 = new SneakersProduction("ВЕРСИЯ 2.0");
$saveManager2 = new SaveManager($production2);
$saveManager2->changeProduction("ВЕРСИЯ 2.1");
$saveManager2->undo();